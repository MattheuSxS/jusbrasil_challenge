from datetime import timedelta
from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.operators.python import BranchPythonOperator
import logging
from src.main import *
from src.transformation import *


logging.basicConfig(
    format='%(asctime)s - %(filename)s:%(lineno)d - %(levelname)s - %(message)s', level='INFO')


default_args = {
    "start_date": days_ago(1),
    "project_id": 'Data_Engineering_Jr',
    "retries": 3,
    "retry_delay": timedelta(minutes=5),
}

with DAG(
        "watchr-dataproc-trigger-dev",
        description="This DAG triggers the pipeline job",
        default_args=default_args,
        schedule_interval="00 8 * * *",
        ) as dag:

    load_rouanet = BranchPythonOperator(
        task_id='load_rouanet',
        description='Loads to the Rouanet base',
        retries=5,
        retry_delay=timedelta(seconds=60),
        python_callable=read_rouanet(),
    )

    load_censo = BranchPythonOperator(
        task_id='load_censo',
        description='Loads to the Censo base',
        retries=5,
        retry_delay=timedelta(seconds=60),
        python_callable=read_censo(),
    )

    main = BranchPythonOperator(
        task_id='main',
        description='Performs the entire transformation process',
        retries=5,
        retry_delay=timedelta(seconds=60),
        python_callable=main(),
    )


load_rouanet >> load_censo >> main 