from setuptools import setup, find_packages
from src.version import VERSION


setup(
    name='jusbrasil-challenge',
    version=VERSION,

    description='Desafio para vaga Data Engineer - Jr',

    author='Matheus Dos Santos',
    author_email='mattheusxs@gmail.com',

    packages=find_packages(exclude=['venv', 'dist', 'docs', 'tests']),
    py_modules=['depara', 'transformation', 'version'],

    install_requires=[
        'pandas==1.5.2',
        'Flask==2.2.2',
        'pytest',
        'coverage',
        'pytest-cov',
        'pytest-html',
        'mock',
        'pylint',
        'autopep8==2.0.0',
        'Unidecode==1.3.6',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.10',
        'Topic :: Scientific/Engineering :: Information Analysis'
    ],
    entry_points='''
        [console_scripts]
        jusbrasil_etl=main
    '''
)
