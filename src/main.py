from pandas import read_csv, DataFrame
import numpy as np
import logging
import os
from src.transformation import *


logging.basicConfig(
    format='%(asctime)s - %(filename)s:%(lineno)d - %(levelname)s - %(message)s', level='INFO')


path_rouanet = os.environ.get("path_rouanet")
path_censo = os.environ.get("path_censo")

def read_rouanet(path:str = path_rouanet) -> DataFrame:
    
    schema_rouanet = dict(
        ano=np.int64,
        estado_ibge=np.int64,
        quantidade=np.int64,
        valor_em_reais=np.float64,
        )

    try:
        data = read_csv(path, dtype=schema_rouanet)
        logging.info("Rouanet CSV file successfully read!")

        return data

    except:
        raise Exception(f'Failed to read Rouanet CSV file!')


def read_censo(path:str = path_censo) -> DataFrame:

    schema_censo = dict(
        estado=str,
        pib=np.int64,
        codigo=np.int64,
        area=np.int64,
        população=np.int64,
        idh=np.int64,
        receita=np.float64,
        despesa=np.float64,
        )

    try:
        data = read_csv(path, dtype=schema_censo)
        logging.info("Censo_Estado CSV file successfully read!")

        return data

    except:
        raise Exception(f"Failed to read Censo_Estado CSV file!")


def main(df_rouanet:DataFrame = read_rouanet(), df_censo:DataFrame = read_censo()) -> None:
    
    logging.info("========== Process started! ==========")

    df_censo = step_1_lower_case(df_censo)
    df_censo = step_2_remove_accent(df_censo)
    df_censo = step_3_change_string(df_censo)
    df_bronze = step_4_join_table(df_censo, df_rouanet)
    df_silver = step_5_create_key(df_bronze)
    df_silver = step_6_remove_duplicate(df_silver)
    df_gold = step_7_remove_lines(df_silver)
    step_8_create_csv(df_gold)

    logging.info("========== Process completed successfully! ==========")


if __name__ == "__main__":
    main()