from unidecode import unidecode
from pandas import DataFrame, merge
from src.depara import depara_states
import logging


logging.basicConfig(
    format='%(asctime)s - %(filename)s:%(lineno)d - %(levelname)s - %(message)s', level='INFO')


def step_1_lower_case(data: DataFrame) -> DataFrame:

    ''' 
        convert uppercase letters to lowercase 
        letters of the state column
    '''
    assert 'estado' in data.columns, \
        'There is no {estado} field in the Dataset'

    data['estado'] = data['estado'].str.lower()
    logging.info("1:Step --> lower case done successfully")

    return data


def step_2_remove_accent(data: DataFrame) -> DataFrame:

    '''
        removes accents from all strings in the state column
    '''
    assert 'estado' in data.columns, \
        'There is no {estado} field in the Dataset'

    data['estado'] = list(map(unidecode, data['estado']))
    logging.info("2:Step --> removes accents from all strings in the state column")

    return data


def step_3_change_string(data: DataFrame) -> DataFrame:

    '''
        change the data in the state column to the acronym of
        the corresponding UF, e.g. Rio de Janeiro becomes RJ
    '''

    assert 'estado' in data.columns, \
        'There is no {estado} field in the Dataset'

    deduplicated_data = set(data['estado'].str.lower())

    for index in deduplicated_data:
        assert index.replace(' ', '') in depara_states().keys(), \
            f'Only the following states are accepted! {depara_states().keys()}'

    data['estado'] = data['estado'].replace(' ', '', regex=True)

    for index in range(len(data['estado'])):
        word = data['estado'][index]

        data._set_value(index, 'estado', depara_states().get(word)[1])
    
    logging.info("3:Step --> change the data in the state column to the acronym of the corresponding UF, e.g. Rio de Janeiro becomes RJ")
    
    return data


def step_4_join_table(df1: DataFrame, df2: DataFrame) -> DataFrame:

    '''
        unify the datasets rouanet.csv and censo_estado.csv
        through the columns Estado_ibge and code, respectively
    '''
    assert 'codigo' in df1.columns, \
        'There is no {codigo} field in the Dataset'
    
    assert 'estado_ibge' in df2.columns, \
        'There is no {estado_ibge} field in the Dataset'

    data = merge(
        left=df1, right=df2,
        left_on='codigo', right_on='estado_ibge'
        )

    logging.info("4:Step --> Join between Rouanet and Census done successfully!")
    
    return data



def step_5_create_key(data: DataFrame) -> DataFrame:

    '''
        create a Natural Key for this data, using the
        columns estado_ibge and valor_em_reais
    '''
    assert 'estado_ibge' in data.columns, \
        'There is no {estado_ibge} field in the Dataset'
    
    assert 'valor_em_reais' in data.columns, \
        'There is no {valor_em_reais} field in the Dataset'
    
    data['key'] = data.agg(lambda x: f"{x['estado_ibge']}{x['valor_em_reais']}", axis=1)

    logging.info("5:Step --> Primary key successfully created!")
    return data


def step_6_remove_duplicate (data: DataFrame) -> DataFrame:

    '''
        remove duplicate lines according to natural key
    '''
    assert 'key' in data.columns, \
        'There is no {key} field in the Dataset'

    data = data.drop_duplicates(subset='key', keep='first', ignore_index=False)
    logging.info("6:Step --> duplicate lines successfully removed!")

    return data


def step_7_remove_lines (data: DataFrame) -> DataFrame:

    '''
        remove rows with valor_em_reais = 0 or quantidade = 0
    '''
    assert 'valor_em_reais' in data.columns, \
        'There is no {valor_em_reais} field in the Dataset'
    
    assert 'quantidade' in data.columns, \
        'There is no {quantidade} field in the Dataset'

    df_delete = data[
        (data['valor_em_reais'] == 0) | (data['quantidade'] == 0)]
    data = data.drop(df_delete.index, axis=0)

    logging.info("7:Step --> Rows with values zero removed successfully!")

    return data


def step_8_create_csv (data: DataFrame, path:str = './generate_csv') -> None:

    '''
        generate a csv
    '''
    try:
        data.to_csv(f'{path}/dados_tratados.csv', index=False)
        logging.info("8:Step --> CSV dados_tratados successfully created!")
    except:
        raise Exception (f'Something went wrong when writing to the file')
