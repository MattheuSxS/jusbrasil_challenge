
def depara_states() -> None:
    states_and_uf = dict(
        acre=("12", "AC", "Acre"),
        alagoas=("27", "AL", "Alagoas"),
        amapa=("16", "AP", "Amapá"),
        amazonas=("13", "AM", "Amazonas"),
        bahia=("29", "BA", "Bahia"),
        ceara=("23", "CE", "Ceara"),
        distritofederal=("53", "DF", "Distrito Federal"),
        espiritosanto=("32", "ES", "Espirito Santo"),
        goias=("52", "GO", "Goiás"),
        maranhao=("21", "MA", "Maranhão"),
        matogrosso=("51", "MT", "Mato Grosso"),
        matogrossodosul=("50", "MS", "Mato Grosso Do Sul"),
        minasgerais=("31", "MG", "Minas Gerais"),
        para=("15", "PA", "Pará"),
        paraiba=("25", "PB", "Paraíba"),
        parana=("41", "PR", "Paraná"),
        pernambuco=("26", "PE", "Pernambuco"),
        piaui=("22", "PI", "Piauí"),
        riodejaneiro=("33", "RJ", "Rio de Janeiro"),
        riograndedonorte=("24", "RN","Rio Grande do Norte"),
        riograndedosul=("43", "RS", "Rio Grande do Sul"),
        rondonia=("11", "RO", "Rondônia"),
        roraima=("14", "RR", "Roraima"),
        santacatarina=("42", "SC", "Santa Catarina"),
        saopaulo=("35", "SP", "São Paulo"),
        sergipe=("28", "SE", "Sergipe"),
        tocantins=("17", "TO", "Tocantins")
    )
    return states_and_uf