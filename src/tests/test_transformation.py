import unittest
from pandas import DataFrame
import numpy as np
from src.transformation import *
from src.depara import depara_states

class TransformationTest(unittest.TestCase):

    def setUp(self):

        self.mandatory_censo = \
            dict(
                estado=str,
                pib=np.int64,
                codigo=np.int64,
                area=np.int64,
                população=np.int64,
                idh=np.int64,
                receita=np.float64,
                despesa=np.float64,
                )

        self.mandatory_rouanet = \
            dict(
                ano=np.int64,
                estado_ibge=np.int64,
                quantidade=np.int64,
                valor_em_reais=np.float64,
            )

        self.dict_censo = \
            dict(
                estado='Rondônia',
                pib=39450587,
                codigo=11,
                area=237765233,
                população=1757589,
                idh=690,
                receita=9122310.72305,
                despesa=7085530.0168,
                )
        
        self.dict_rouanet = \
            dict(
                ano=2013,
                estado_ibge=11,
                quantidade=4,
                valor_em_reais=271753.93,
            )

        self.data_censo = DataFrame([self.dict_censo])
        self.data_rouanet = DataFrame([self.dict_rouanet])


    # ========================================================================================================== #
    #                                           check  of the functions                                          #
    # ========================================================================================================== #


    def test_check_function_step1(self) -> None:

        '''
            Drop column state
        '''
        fixture = self.data_censo.copy()
        fixture = fixture.drop("estado", axis='columns')

        with self.assertRaises(AssertionError) as ctx:
            step_1_lower_case(fixture)

        self.assertEqual("There is no {estado} field in the Dataset", str(ctx.exception))


    def test_check_function_step2(self) -> None:

        '''
            Drop column state
        '''
        fixture = self.data_censo.copy()
        fixture = fixture.drop("estado", axis='columns')

        with self.assertRaises(AssertionError) as ctx:
            step_2_remove_accent(fixture)

        self.assertEqual("There is no {estado} field in the Dataset", str(ctx.exception))


    def test_check_function_step3(self) -> None:

        '''
            Drop column state
        '''
        fixture = self.data_censo.copy()
        fixture = fixture.drop("estado", axis='columns')

        with self.assertRaises(AssertionError) as ctx:
            step_3_change_string(fixture)

        self.assertEqual("There is no {estado} field in the Dataset", str(ctx.exception))


        '''
            Does not match state
        '''
        fixture = self.data_censo.copy()
        with self.assertRaises(AssertionError) as ctx:
            step_3_change_string(fixture)

        self.assertEqual(f'Only the following states are accepted! {depara_states().keys()}', str(ctx.exception))


    def test_check_function_step4(self) -> None:

        '''
            Drop column codigo
        '''
        fixture_censo = self.data_censo.copy()
        fixture_rouanet = self.data_rouanet.copy()
        fixture_censo = fixture_censo.drop('codigo', axis='columns')

        with self.assertRaises(AssertionError) as ctx:
            step_4_join_table(fixture_censo, fixture_rouanet)

        self.assertEqual('There is no {codigo} field in the Dataset', str(ctx.exception))

        '''
            Drop column estado_ibge
        '''
        fixture_censo = self.data_censo.copy()
        fixture_rouanet = self.data_rouanet.copy()
        fixture_rouanet = fixture_rouanet.drop('estado_ibge', axis='columns')

        with self.assertRaises(AssertionError) as ctx:
            step_4_join_table(fixture_censo, fixture_rouanet)

        self.assertEqual('There is no {estado_ibge} field in the Dataset', str(ctx.exception))


    def test_check_function_step5(self) -> None:

        '''
            Drop column estado_ibge
        '''
        fixture_censo = self.data_censo.copy()
        fixture_rouanet = self.data_rouanet.copy()
        fixture = step_4_join_table(fixture_censo, fixture_rouanet)
        fixture = fixture.drop('estado_ibge', axis='columns')

        with self.assertRaises(AssertionError) as ctx:
            step_5_create_key(fixture)

        self.assertEqual('There is no {estado_ibge} field in the Dataset', str(ctx.exception))

        '''
            Drop column valor_em_reais
        '''
        fixture_censo = self.data_censo.copy()
        fixture_rouanet = self.data_rouanet.copy()
        fixture = step_4_join_table(fixture_censo, fixture_rouanet)
        fixture = fixture.drop('valor_em_reais', axis='columns')

        with self.assertRaises(AssertionError) as ctx:
            step_5_create_key(fixture)

        self.assertEqual('There is no {valor_em_reais} field in the Dataset', str(ctx.exception))


    def test_check_function_step6(self) -> None:

        '''
            Basis without primary key
        '''
        fixture_censo = self.data_censo.copy()
        fixture_rouanet = self.data_rouanet.copy()
        fixture = step_4_join_table(fixture_censo, fixture_rouanet)

        with self.assertRaises(AssertionError) as ctx:
            step_6_remove_duplicate(fixture)

        self.assertEqual('There is no {key} field in the Dataset', str(ctx.exception))


    def test_check_function_step7(self) -> None:

        '''
            Drop column estado_ibge
        '''
        fixture_censo = self.data_censo.copy()
        fixture_rouanet = self.data_rouanet.copy()
        fixture = step_4_join_table(fixture_censo, fixture_rouanet)
        fixture = fixture.drop('valor_em_reais', axis='columns')

        with self.assertRaises(AssertionError) as ctx:
            step_7_remove_lines(fixture)

        self.assertEqual('There is no {valor_em_reais} field in the Dataset', str(ctx.exception))

        '''
            Drop column valor_em_reais
        '''
        fixture_censo = self.data_censo.copy()
        fixture_rouanet = self.data_rouanet.copy()
        fixture = step_4_join_table(fixture_censo, fixture_rouanet)
        fixture = fixture.drop('quantidade', axis='columns')

        with self.assertRaises(AssertionError) as ctx:
            step_7_remove_lines(fixture)

        self.assertEqual('There is no {quantidade} field in the Dataset', str(ctx.exception))


    def test_check_function_step8(self) -> None:

        '''
            invalid path
        '''
        fixture = self.data_censo.copy()

        with self.assertRaises(Exception) as ctx:
            step_8_create_csv(fixture, 'path/erro')

        self.assertEqual(f'Something went wrong when writing to the file', str(ctx.exception))
