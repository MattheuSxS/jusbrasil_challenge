import unittest
from pandas import DataFrame
import numpy as np
from src.transformation import *
from src.main import main, read_rouanet, read_censo


class MainTest(unittest.TestCase):

    def setUp(self):

        self.mandatory_censo = \
            dict(
                estado=str,
                pib=np.int64,
                codigo=np.int64,
                area=np.int64,
                população=np.int64,
                idh=np.int64,
                receita=np.float64,
                despesa=np.float64,
                )

        self.mandatory_rouanet = \
            dict(
                ano=np.int64,
                estado_ibge=np.int64,
                quantidade=np.int64,
                valor_em_reais=np.float64,
            )

        self.dict_censo = \
            dict(
                estado='Rondônia',
                pib=39450587,
                codigo=11,
                area=237765233,
                população=1757589,
                idh=690,
                receita=9122310.72305,
                despesa=7085530.0168,
                )
        
        self.dict_rouanet = \
            dict(
                ano=2013,
                estado_ibge=11,
                quantidade=4,
                valor_em_reais=271753.93,
            )

        self.data_censo = DataFrame([self.dict_censo])
        self.data_rouanet = DataFrame([self.dict_rouanet])

    
    def test_check_function_main(self):

        self.assertIsNone(main(self.data_rouanet, self.data_censo), \
            'Process failed!')


    def test_check_function_read_rouanet(self):

        with self.assertRaises(Exception) as ctx:
            read_rouanet('path/error')

        self.assertEqual(f'Failed to read Rouanet CSV file!', str(ctx.exception))


    def test_check_function_read_censo(self):

        with self.assertRaises(Exception) as ctx:
            read_censo('path/error')

        self.assertEqual(f"Failed to read Censo_Estado CSV file!", str(ctx.exception))