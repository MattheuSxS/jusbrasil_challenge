# Desafio de Data Engineering
Este projeto manipular um ETL usando tabelas brutas censo_estado.csv e rouanet.csv

## Resumo Geral
Para realização do desafio foi utilizado Python 3.10.8. Para executar o código localmente é necessário fazer o download do projeto dando um `Git clone` e instalar as dependências necessárias para a execução do código. Todos os comandos abaixo são executados no **CMD**, na **pasta raiz do projeto**.
Recomendo ao usuário usar uma das opções abaixo como ferramenta de criação do ambiente:

- Anaconda/Miniconda:

        Conda create –n $(basename $(pwd)) python=3.10
        conda activate $(basename $(pwd))

- Pyenv

        python3.10 -m venv app_env
        source app_env/bin/activate

Dentro da pasta raiz Jusbrasil_challenge/src
temos dois arquivos a qual é usado para instalar as bibliotecas necessárias para roda o código são eles;

- requirements.txt
    -  Este é usando pelo CI/CD do gitlab
- requirements-dev.txt
    -  Este é usado pelo usuário

Para instalar as dependencias necessárias para rodar o código use o seguinte comando;

    pip install -r requirements-dev.txt
    ou
    pip3 install -r requirements-dev.txt

Leva alguns minutos para completar toda a instalação e o ambiente está pronto para uso.
Às vezes quando e criado um ambiente virtual o Python pode não reconhecer o diretório e apresentar alguns erros estranhos, então é recomendado usar o comando abaixo na pasta raiz do projeto;

    export PYTHONPATH=$(pwd)

Caso execute o código com os valores default é necessário passar o caminho dos arquivos para seu ambiente usando os seguintes comandos.

    export path_censo="../justbrasil_challenge/configs/censo_estado.csv"
    export path_rouanet="../justbrasil_challenge/configs/rouanet.csv"   


Com o ambiente criado e dependências instaladas, o usuário pode mandar o seguinte comando na pasta raiz jusbrasil_challenge/src.

    python main.py
    ou
    Python3 main.py


## Agregador de eventos de pipeline
Este Workflow é implementado em Python, podendo ser orquestrado no airflow local ou Cloud (Google Cloud Compose) . Ele transforma os dados e salva eles em um arquivo csv de nome dados_tratados.csv. O arquivo final fica localizado dentro da pasta raiz `src/generate_csv/*.csv`

Conforme ilustrado na figura abaixo, esse pipeline possui oito camadas:

- Raw: dados carregados de `censo_estado.csv` and `rouanet.csv`
- Stage:
    - 1: `step_1_lower_case` -> convert uppercase letters to lowercase letters of the state column
    - 2: `step_2_remove_accent` -> removes accents from all strings in the state column
    - 3: `step_3_change_string` -> change the data in the state column to the acronym of the corresponding UF, e.g. Rio de Janeiro becomes RJ
    - 4: `step_4_join_table` -> unify the datasets rouanet.csv and censo_estado.csv through the columns Estado_ibge and code, respectively
    - 5: `step_5_create_key` -> create a Natural Key for this data, using the columns estado_ibge and valor_em_reais
    - 6: `step_6_remove_duplicate` -> Remove duplicate lines according to natural key
    - 7: `step_7_remove_lines` -> Remove rows with valor_em_reais = 0 or quantidade = 0
    - 8: `step_8_create_csv` -> Generate a csv


![alt text](JusBrasil_Pipeline.drawio.png)

## Estrutura de código
```
.
├── Doc
│   ├── HISTORY.md
│   ├── JusBrasil_Pipeline.drawio.png
├── flow
│   ├── dags
|   |  ├── scripts
|   |  ├── Pipeline-trigger-dev.py
|   |  ├── Pipeline-trigger-prd.py
│   ├── data
│   └── logs
│   └── tmp
├── jusbrasil_challenge
│   ├── configs
|   |  ├── censo_estado.csv
|   |  ├── rouanet.csv
│   ├── tests
|   |  ├── __init__.py
|   |  ├── test_censo.py
|   |  ├── test_rouanet.py
|   |  ├── test_validator.pyy
│   └── validates
|   |  ├── __init__.py
|   |  ├── censo.py
|   |  ├── rouanet.py
|   |  ├── .coveragerc
|   |  ├── Makefile
|   |  ├── validator.py
├── src
│   ├── generate_csv
│   |   ├── dados_tratados.csv
│   ├── tests
│   |   ├── test_main.py
│   |   ├── test_transformation.py
│   ├── __init__.py
│   ├── .coveragerc
│   ├── depara.py
│   ├── main.py
│   ├── Makefile
│   ├── requirements-dev.txt
│   ├── requirements.txt
│   ├── setup.py
│   ├── transformation.py
│   ├── version.py
├── .gitignor
├── .gitlab-ci.yml
├── .python-version
├── Makefile
└── README.md
```


## Como esses pipelines são executados
O [airflow Scheduler](https://airflow.apache.org/docs/apache-airflow/2.4.3/concepts/scheduler.html). Atualmente, essas agregações são executadas uma vez por dia:

- Every day at 8:00 A.M. (America/Sao_Paulo).

TODO: Fazer a implementação

## Testes Automatizados

O projeto tem duas pastas de testes localizadas em;

- Jusbrasil_challenge/Jusbrasil_challenge
    - tests
	- validates

- Jusbrasil_challenge/src
	- tests

- O usuário executa esses testes entrando em um dos dois diretórios citado acima e executando um dos comandos abaixo;
    - make test
    - make coverage

Os testes estão automatizados no CI/CD do Gitlab


## Gitlab CI/CD
O projeto é deployado pelo CI/CD do Gitlab no repositório `https://gitlab.com/MattheuSxS/jusbrasil_challenge`. Há três etapas a serem feitas para um deploy ser bem sucedido! São elas;

- Stages:
    - run-coverage --> Roda os testes no código
    - validate-config-files --> Faz a validação dos arquivos
    - application-deploy --> Executa o código


## Referências

- GitLab:
    - [Documento GitLab](https://docs.gitlab.com)
    - [Comando GitLab Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
    - [GitLab CI/CD](https://docs.gitlab.com/ee/ci/ )

- Python:
    - [Documento Python](https://docs.python.org/3.10/)
    - [Documento Pandas](https://pandas.pydata.org/docs/)
    - [Documento PySpark](https://spark.apache.org/docs/latest/api/python/index.html)
    - [Documento Pytest](https://docs.pytest.org/en/7.2.x/contents.html)
    - [Documento Coverage](https://coverage.readthedocs.io/en/6.5.0/)

- Airflow:
    - [Documento Airflow](https://airflow.apache.org/docs/)
    - [Scheduler Airflow](https://airflow.apache.org/docs/apache-airflow/1.10.13/scheduler.html)

- Apache Spark:
    - [Documento Apache Spark](https://spark.apache.org/docs/latest/)

- Google Cloud:
    - [Documento Composer](https://cloud.google.com/composer/docs/run-apache-airflow-dag)

- Docker:
    - [Documento Docker](https://docs.docker.com)
    - [Docker Airflow](https://hub.docker.com/r/apache/airflow)

- Makefile
    - [Documento Makefile](https://www.gnu.org/software/make/manual/make.html)
    - [Documento Tutorial](https://makefiletutorial.com)


| Versão Do Documento |        Editor      |    Data    |
|        :---:        |        :---:       |    :---:   |
|        1.0.0        | Matheus Dos Santos | 03/12/2022 |