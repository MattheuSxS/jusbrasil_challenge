.PHONY: build-function deploy_airflow

build-function:
	cp src/*.py ./flow/dags/scripts \
	&& cp src/requirements-dev.txt ./flow/dags/scripts


deploy_airflow:
	docker run -d -p 8080:8080 -v "$PWD/flow/dags:/opt/airflow/dags/" \
	--entrypoint=/bin/bash \
	--name airflow apache/airflow:2.3.4-python3.10 \
	-c '(airflow db init && \
		airflow users create --username admin --password abc123 --firstname Matheus --lastname Santos --role Admin --email mattheusxs@gmail.com
		); \
	airflow webserver & \
	airflow scheduler \
	'