## TODO
- Subir um docker do Airflow e Schedular o job:
    - Falta subir a docker e configurar a dag corretamente.
    - Subir o Airflow → Pode ser automatizado usando CI/CD + Makefile. Caso for feito no Google Cloud Compose, terá que automatizar a criação do ambiente usando o terraform + CI/CD

- Criar novos cenários de testes:
    - Eh possível se aprofundar mais nos testes unitários deixando eles bem mais completos

- Fazer o mesmo pipeline usando pyspark para Extrair/Transformar/Carregar:
    - montar um ambiente adequado para rodar pypask
    - modicar o código tirando o pandas e usando spark para a maiorias das transformações
    - aplicar testes unitários para spark

- Utilizar a biblioteca [Click](https://click.palletsprojects.com/en/8.1.x/) do python para aumentar o leque de opções na hora de selecionar 

    - Diretórios
    - Arquivos
    - Argumentos passado para as funções 
