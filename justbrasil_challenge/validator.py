import os
import logging
import argparse
from pandas import DataFrame, read_csv


logging.basicConfig(
    format='%(asctime)s - %(filename)s:%(lineno)d - %(levelname)s - %(message)s', level='INFO')


def is_valid(data: DataFrame) -> None:
    if data.empty:
        raise Exception('empty dataframe')


if __name__ == '__main__':

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "-c", "--check", help="execute validates for config files.", required=True, action="store_true"
        )

        args = parser.parse_args()

        if args.check:
            configs_path = os.path.join(os.getcwd(), 'configs')

            files = os.listdir(configs_path)

            for filename in files:
                logging.info(f"ready for validation file: {filename}")

                with open(os.path.join(configs_path, filename), mode='r', encoding='utf-8') as f:
                    data = read_csv(f)

                if is_valid(data):
                    logging.info(f"sucessfull validation for: {filename}")


    except Exception as error:
        raise(f"Invalid csv file! Error: {error}.")
