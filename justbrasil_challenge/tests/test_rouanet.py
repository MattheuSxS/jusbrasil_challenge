import unittest
from pandas import DataFrame
import numpy as np
from random import choice
from validates.rouanet import *
from src.depara import depara_states

class RouanetTest(unittest.TestCase):

    def setUp(self):

        self.mandatory = \
            dict(
                ano=np.int64,
                estado_ibge=np.int64,
                quantidade=np.int64,
                valor_em_reais=np.float64,
                )

        self.dict_ = \
            dict(
                ano=2013,
                estado_ibge=17,
                quantidade=4,
                valor_em_reais=271753.93,
                )

        self.data = DataFrame([self.dict_])


    # ========================================================================================================== #
    #                                       check  of the fields  mandatory                                      #
    # ========================================================================================================== #

    def test_check_mandatory_field_all(self):
        fixture = self.data.copy()

        data_columns = fixture.columns
        delete_columns = choice(list(data_columns))
        fixture.drop(delete_columns, inplace=True, axis=1)

        with self.assertRaises(AssertionError) as ctx:
            check_mandatory_field(dict(fixture.dtypes))

        self.assertEqual(f"Expected mandatory argument ['{delete_columns}'] in received args: {self.mandatory.keys()}.", str(ctx.exception))


    # ========================================================================================================== #
    #                                   verification of required field types                                     #
    # ========================================================================================================== #

    def test_check_type_all_required_fields(self):
        fixture = self.data.copy()

        fixture['estado_ibge'] = np.float64
        with self.assertRaises(AssertionError) as ctx:
            check_mandatory_field(dict(fixture.dtypes))

        self.assertEqual(f"Expected argument ['estado_ibge'] is {np.int64}.", str(ctx.exception))


    # ========================================================================================================== #
    #                                              check column value                                            #
    # ========================================================================================================== #


    def test_check_column_value_codigo(self):
        fixture = self.data.copy()

        fixture['codigo'] = 1111
        list_values = []
        for key, values in depara_states().items():
            list_values.append(int(values[0]))

        with self.assertRaises(Exception) as ctx:
            check_column_codigo(fixture)

        self.assertEqual(f"The code column only accepts values: {list_values}.", str(ctx.exception))