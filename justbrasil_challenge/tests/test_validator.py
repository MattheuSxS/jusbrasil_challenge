import unittest
from pandas import DataFrame
import numpy as np
from justbrasil_challenge.validator import is_valid


class ValidatorTest(unittest.TestCase):

    def setUp(self):

        self.mandatory = \
            dict(
                ano=np.int64,
                estado_ibge=np.int64,
                quantidade=np.int64,
                valor_em_reais=np.float64,
                )

        self.dict_ = \
            dict(
                ano=2013,
                estado_ibge=17,
                quantidade=4,
                valor_em_reais=271753.93,
                )

        self.data = DataFrame([self.dict_])


    # ========================================================================================================== #
    #                                         check if dataframe is empty                                        #
    # ========================================================================================================== #

    def test_check_mandatory_field_all(self):
        fixture = DataFrame()


        with self.assertRaises(Exception) as ctx:
            is_valid(fixture)

        self.assertEqual(f"empty dataframe", str(ctx.exception))