from pandas import DataFrame
import numpy as np
from src.depara import depara_states


def check_mandatory_field(data: DataFrame) -> None:
    mandatory = \
        dict(
        estado=object,
        pib=np.int64,
        codigo=np.int64,
        area=np.int64,
        população=np.int64,
        idh=np.int64,
        receita=np.float64,
        despesa=np.float64,
            )

    data_keys = data.keys()

    for key in mandatory.keys():
        assert key in data_keys, \
            f"Expected mandatory argument ['{key}'] in received args: {mandatory.keys()}."


    for key, values in mandatory.items():
        data_value = data.get(key)

        assert data_value == values, \
            f"Expected argument ['{key}'] is {values}."


def check_column_codigo(data:DataFrame) -> None:

    states_list = []
    for value_ in data.get('codigo'):
        states_list.append(value_)


    list_values = []
    for key, values in depara_states().items():
        list_values.append(int(values[0]))

    for value in states_list:
        if value not in list_values:
            raise Exception(f"The code column only accepts values: {list_values}.")

